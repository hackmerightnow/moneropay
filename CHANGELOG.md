# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.3.0] - 2023-07-11
- Export callback response struct under `/pkg/model`.

## [2.2.2] - 2023-04-02
- Fixed bug where MoneroPay attempts to callback when callback URL isn't supplied.
- Exported API structs under `/pkg/model`.

## [2.2.1] - 2023-02-18

### Fixed
- Upgraded [pgx](https://github.com/jackc/pgx) PostgreSQL driver library to v5 from v4. Dead connection and broken pipe handling was added.
